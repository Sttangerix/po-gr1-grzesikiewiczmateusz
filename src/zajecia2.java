import java.util.Scanner;
import java.util.Random;
public class zajecia2 {
    zajecia2() {
        Scanner odczyt = new Scanner(System.in);
        this.mnozenie(odczyt);

    }

    public void wyswietl(int[] t) {
        for (int i = 0; i < t.length; i++) {
            System.out.print(t[i] + ", ");
        }
        System.out.println();
    }

    public int[] generuj(int n) {
        int[] tab = new int[n];
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = (int) (Math.random() * 2 * -998) + 999;
        }
        return tab;
    }

    public void zad1(Scanner odczyt) {
        int n = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        wyswietl(t);
    }

    public void zad1b(Scanner odczyt) {
        int n = odczyt.nextInt();
        int dod = 0, ujem = 0, zer = 0;
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        for (int i = 0; i < t.length; i++) {
            if (t[i] < 0) {
                ujem++;
            } else if (t[i] > 0) {
                dod++;
            } else if (t[i] == 0) {
                zer++;
            }
            System.out.print("Dodatnich jest: " + dod + "  ,ujemnych jest: " + ujem + " natomiast zer jest: " + zer);
        }
    }

    public void zad1a(Scanner odczyt) {
        int n = odczyt.nextInt();
        int parz = 0, nieparz = 0;
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        for (int i = 0; i < t.length; i++) {
            if (t[i] % 2 == 0) {
                parz++;
            } else {
                nieparz++;
            }
        }
        System.out.print("Parzystych jest: " + parz + " a nieparzystych jest: " + nieparz);
    }

    public void zad1c(Scanner odczyt) {

        int n = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);

        int max = t[0], ilosc = 1;
        for (int i = 1; i < t.length; i++) {
            if (t[i] > max) {
                max = t[i];
                ilosc = 1;
            } else if (t[i] == max) {
                ilosc++;
            }

        }
        System.out.print("Maksymalny element tablicy to: " + max + " i występuje: " + ilosc + " razy.");
    }

    public void zad1d(Scanner odczyt) {
        int sumd = 0, sumu = 0;
        int n = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        for (int i = 0; i < t.length; i++) {
            if (t[i] > 0) {
                sumd += t[i];
            } else if (t[i] < 0) {
                sumu += t[i];
            }
        }
        System.out.print("Suma liczb ujemnych wynosi: " + sumu + "\nSuma liczb dodatnich wynosi: " + sumd);
    }

    public void zad1e(Scanner odczyt) {
        int ilosc = 0, max = 0;
        int n = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        wyswietl(t);
        for (int i = 0; i < t.length; i++) {
            if (t[i] > 0) {
                ilosc++;
                max = ilosc;
            } else if (t[i] <= 0) {
                if (max < ilosc) {
                    max = ilosc;

                }
                ilosc = 0;
            }
        }
        System.out.print("Najdłuższy fragment dodatnich elementów wynosi: " + max);
    }

    public void zad1f(Scanner odczyt) {
        int n = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        for (int i = 0; i < t.length; i++) {
            if (t[i] >= 0) {
                t[i] = 1;
            } else if (t[i] < 0) {
                t[i] = -1;
            }
        }
        wyswietl(t);
    }

    public void zad1g(Scanner odczyt) {
        int n = odczyt.nextInt();
        int l = odczyt.nextInt();
        int r = odczyt.nextInt();
        if (n < 1 || n > 100) {
            return;
        }
        int[] t = generuj(n);
        wyswietl(t);
        if (l <= 1 || l > n || r <= 1 || r > n) {
            return;
        }
        int tm = t[l];
        t[l] = t[r];
        t[r] = tm;
        wyswietl(t);

    }

    public static void generuj1(int tab[], int start, int end) {
        Random rand = new Random();
        for (int i = start; i < end; i++) {
            tab[i] = rand.nextInt();
        }
    }
    public static  void wypisz(int[] tab){
        for(int el:tab){
            System.out.print(el+" | ");
        }
        System.out.print("");
    }
    public void wypiszmacierz(int[][]tab) {
        System.out.println("Macierz: ");
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.print(tab[i][j]);
            }
            System.out.println();
        }
    }

    public static int ileNieparzystych(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), nieparz = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] % 2 == 0) {
                nieparz++;

            }
        }
        return nieparz;
    }

    public static int ilearzystych(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), parz = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] % 2 == 0) {
                parz++;
            }
        }
        return parz;
    }

    public static int ileDodatnich(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), dod = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] > 0) {
                dod++;
            }

        }
        return dod;
    }

    public static int ileUjemnych(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), uje = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] < 0) {
                uje++;
            }

        }
        return uje;
    }

    public static int ilezer(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), zer = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] == 0) {
                zer++;
            }

        }
        return zer;
    }

    public static int ileMaksymalnych(int tab[], Scanner odczyt) {
        int n = odczyt.nextInt(), uje = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        int max = t[0], ilosc = 1;
        for (int i = 1; i < t.length; i++) {
            if (t[i] > max) {
                max = t[i];
                ilosc = 1;
            } else if (t[i] == max) {
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int sumaDodatnich(int[] tab, Scanner odczyt) {
        int n = odczyt.nextInt(), sumd = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] > 0) {
                sumd += t[i];
            }
        }
        return sumd;
    }
    public static int sumaUjemnych(int[] tab, Scanner odczyt) {
        int n = odczyt.nextInt(), sumu = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] < 0) {
                sumu += t[i];
            }
        }
        return sumu;
    }
    public static int dlugoscMaksymalnegoCiaguDodatniego(int[] tab,Scanner odczyt) {
        int n = odczyt.nextInt(), ilosc = 0, max = 0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] > 0) {
                ilosc++;
                if(t[i]>0) {
                    max = ilosc;
                }
            } else if (t[i] <= 0) {

                ilosc = 0;
            }
        } return max;
        }


    public static void signum(int[] tab,Scanner odczyt){
        int n = odczyt.nextInt(), ilosc = 0,max=0;
        int[] t = new int[n];
        generuj1(t, -999, 999);
        for (int i = 0; i < t.length; i++) {
            if (t[i] >= 0) {
                t[i] = 1;
            } else if (t[i] < 0) {
                t[i] = -1;
            }
        }
        wypisz(t);
    }
    public static void odwrocFragment(int[] tab,int lewy, int prawy,Scanner odczyt){
        int n = odczyt.nextInt(), ilosc = 0,max=0;
        int[] t = new int[n];
        if (n < 1 || n > 100) {
            return;
        }
        generuj1(t, -999, 999);
        wypisz(t);
        if (lewy <= 1 || lewy > n || prawy <= 1 || prawy > n) {
            return;
        }
        int tm = t[lewy];
        t[lewy] = t[prawy];
        t[prawy] = tm;
        wypisz(t);
    }
    public int[][] mnozenie(Scanner odczyt) {
        int m = odczyt.nextInt();
        int n = odczyt.nextInt();
        int k = odczyt.nextInt();
        int[][] c=new int[0][0];
        while (m > 10 || m < 1 || n > 10 || n < 1 || k > 10 || k < 1) {
            System.out.print("Złe dane");
        }
        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        wypiszmacierz(a);
        if (a[0].length == b.length) {

            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < b[0].length; j++) {
                    int temp = 0;
                    for (int w = 0; w < b.length; w++) {
                        temp += a[i][w] * b[w][j];
                    }
                    c[i][j] = temp;
                }

            }
        }
        return c;
    }
}

