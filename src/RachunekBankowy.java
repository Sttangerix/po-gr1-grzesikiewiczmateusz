public class RachunekBankowy {
    public static double rocznaStopaProcentowa;
    private double saldo = 0;

    RachunekBankowy(double value){
        saldo = value;
    }

    public void obliczMiesieczneOdsetki(){
        saldo += (this.saldo * rocznaStopaProcentowa) / 12;
    }
    public static void setRocznaStopaProcentowa(double value){
        rocznaStopaProcentowa = value;
    }
    public double getSaldo(){
        return saldo;
    }
}
