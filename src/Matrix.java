import java.util.Random;

public class Matrix {
    private int[][] matrix;
    public int width       = 0;
    public int height      = 0;

    Matrix(int w, int h){
        matrix  = new int[w][h];
        width   = w;
        height  = h;
    }

    public void fill(){
        Random rand = new Random();

        for (int i = 0; i < width; i++){
            for (int j = 0; j < height; j++){
                matrix[i][j] = rand.nextInt(10);
            }
        }
    }
    public void set(int x, int y, int val){
        matrix[x][y] = val;
    }

    public static Matrix product(Matrix a, Matrix b){
        if (a.height != b.width){
            System.out.println("Nie mozna pomnozyc macierzy!");
            return new Matrix(0 ,0);
        }
        Matrix c = new Matrix(a.width, b.height);
        int sum = 0;

        for (int i = 0; i < a.width; i++){
            for (int j = 0; j < b.height; j++){
                for (int k = 0; k < b.width; k++){
                    sum += a.matrix[i][k] * b.matrix[k][j];
                }
                c.set(i, j, sum);
                sum = 0;
            }
        }
        return c;
    }

    public void print(){
        for (int i = 0; i < matrix.length; i++){
            System.out.print("| [ ");
            for (int j = 0; j < matrix[0].length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("] |");
            System.out.println();
        }
    }
}