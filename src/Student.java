public class Student extends Osoba10 implements Comparable<Osoba10>, Cloneable {
    private double sredniaOcen;
    public Student(String Nazwisko, int Rok,int Miesiac, int Dzien, double sredniaOcen){
        super(Nazwisko,Rok,Miesiac,Dzien);
        this.sredniaOcen=sredniaOcen;
    }
    public int compareTo(Osoba10 o){
        Student otherStudent=(Student) o;
        if((super.compareTo(otherStudent))==0){
            if((this.sredniaOcen==otherStudent.sredniaOcen)){
                return 0;
            }
            else if(this.sredniaOcen<otherStudent.sredniaOcen){
                return -1;
            }
            else return 1;
        }
        else return  super.compareTo(otherStudent);
    }
    public double getSredniaOcen(){

        return this.sredniaOcen;
    }

    public String toString () {
        String nazwa = "";
        nazwa += getClass().getName();
        return (nazwa + " " + "[Nazwisko: " + getNazwisko() + "], [data urodzenia: " + getDataUrodzenia()+ "]\n");

    }


}
