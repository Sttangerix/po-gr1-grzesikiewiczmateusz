public class lista6 {
    lista6(){
        RachunekBankowy.setRocznaStopaProcentowa(0.04);

        RachunekBankowy saver1 = new RachunekBankowy(2000.0);
        RachunekBankowy saver2 = new RachunekBankowy(3000.0);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.print(saver1.getSaldo());
        System.out.println("\n");
        System.out.print(saver2.getSaldo());
    }

}
