import java.time.LocalDate;
import java.time.Period;

public class Osoba10 implements Comparable<Osoba10> ,Cloneable  {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba10(String Nazwisko, int Rok, int Miesiac, int Dzien){
        this.nazwisko = Nazwisko;
        this.dataUrodzenia = LocalDate.of(Rok, Miesiac, Dzien);
    }
    public String toString () {
            String nazwa = "";
            nazwa += getClass().getName();
            return (nazwa + " " + "[Nazwisko: " + nazwisko + "], [data urodzenia: " + dataUrodzenia + "]\n");

        }
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Osoba10 o = (Osoba10) obj;
        if (nazwisko == null) { if (o.nazwisko != null) return false; }
        else if (!nazwisko.equals(o.nazwisko)) return false;
        if (dataUrodzenia != o.dataUrodzenia) return false;
        return true;
    }
    public int compareTo(Osoba10 o)
    {
        if ((this.nazwisko.compareTo(o.nazwisko)) == 0) {
            if ((this.dataUrodzenia.compareTo(o.dataUrodzenia) == 0)) {
                return 0;
            }
            else return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return this.nazwisko.compareTo(o.nazwisko);
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }
}
