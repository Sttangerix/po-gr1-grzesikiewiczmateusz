import java.time.LocalDate;
import java.time.Period;


public class Osoba9 implements Comparable<Osoba9>  {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    Osoba9(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
        }


    public int compareTo(Osoba9 o){
        if((this.nazwisko.compareTo(o.nazwisko)) == 0) {
            if ((this.dataUrodzenia.compareTo(o.dataUrodzenia)) == 0) {
                return 0;
            } else return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        } else return this.nazwisko.compareTo(o.nazwisko);
    }



    public String toString(){
        String nazwa="";
        nazwa+=getClass().getName();
        return (nazwa+" "+"[Nazwisko: "+nazwisko+"], [data urodzenia: "+dataUrodzenia.toString()+"]\n");
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        Osoba9 other = (Osoba9) o;
        if (nazwisko == null) if (other.nazwisko != null) return false;
        else if (!nazwisko.equals(other.nazwisko)) return false;
        if (this.dataUrodzenia != other.dataUrodzenia) return false;
        return true;
    }

    public int ileLat(Osoba9 o){
        Period i=Period.between(o.dataUrodzenia,LocalDate.now());
        return i.getYears();
    }
    public int ileMiesiecy(Osoba9 o){
        Period i=Period.between(o.dataUrodzenia,LocalDate.now());
        return i.getMonths();
    }
    public int ileDni(Osoba9 o){
        Period i=Period.between(o.dataUrodzenia,LocalDate.now());
        return i.getDays();
    }

}
