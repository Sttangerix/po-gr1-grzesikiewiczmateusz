import java.util.Scanner;

public class lista4 {
    lista4(){
        Scanner odczyt = new Scanner(System.in);
        //char c=odczyt.next().charAt(0);

        String a=odczyt.nextLine();

       // int wynik=this.countChar(a,c);
        String wynik=this.nice(a);
        System.out.print(wynik);
    }
    static int countChar(String str,char c){
        int licznik=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==c){
                licznik++;
            }
        }
        return licznik;
    }

    public static int countSubStr(String str, String substr) {

        int ilosc = 0;
        for (int i = 0; i < str.length() - substr.length() + 1; i++)
            if (str.substring(i, i + substr.length()).equals(substr))
                ilosc++;
        return ilosc;
    }
    public static String middle(String str) {

        if (str.length() % 2 != 0)
            return String.valueOf(str.charAt(str.length() / 2 + 1));
        else
            return str.substring(str.length() / 2, str.length() / 2 + 1);

    }

    public static String repeat(String str, int n) {

        for (int i = 0; i < n; i++)
            str += str;

        return str;
    }
    public static int[] where(String str, String substr) {

        int tab[] = new int[str.length()];
        int j = 0;
        for (int i = 0; i < str.length() - substr.length() + 1; i++)
            if (str.substring(i, i + substr.length()).equals(substr)) {
                tab[j] = i;
                j++;
            }


        return tab;
    }

    public static String change(String str) {

        StringBuffer stringbuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++)
            if (Character.isUpperCase(str.charAt(i)))
                stringbuffer.append(Character.toLowerCase(str.charAt(i)));
            else if (Character.isLowerCase(str.charAt(i)))
                stringbuffer.append(Character.toUpperCase(str.charAt(i)));

        return stringbuffer.toString();
    }
    public static String nice(String str){
        StringBuffer stringbuffer = new StringBuffer();
        for(int i=str.length();i>=0;i--){
            if(i%3==0){
                stringbuffer.append("1.");
            }
            else{
                stringbuffer.append("1");
            }
        }
        return stringbuffer.toString();
    }
}

