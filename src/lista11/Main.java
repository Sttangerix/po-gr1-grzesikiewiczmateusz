package lista11;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Pair<String> test = new Pair<String>();
        test.setFirst("5");
        test.setSecond("7");
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());
        test.swap();
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());


        PairUtil<String> test1 = new PairUtil<String>();
        test = test1.swap(test);
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());

        Item[] unsorted = new Item[4];
        unsorted[0] = new Item(100);
        unsorted[1] = new Item(0);
        unsorted[2] = new Item(200);
        unsorted[3] = new Item(50);
        Arrays.sort(unsorted);

        for (Item element : unsorted){
            System.out.println(element);
        }

        Item[] sorted = new Item[4];
        sorted[0] = new Item(0);
        sorted[1] = new Item(50);
        sorted[2] = new Item(100);
        sorted[3] = new Item(200);

        Arrays.sort(sorted);

        for (Item element : sorted) {
            System.out.println(element);
        }

    }
}