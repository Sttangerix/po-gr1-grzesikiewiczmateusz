package lista11;

class Item<T> implements Comparable<Item<T>> {

    public Item(T wartosc){
        this.wartosc = wartosc;
    }

    public int compareTo(Item o)
    {
        int x = (int) o.wartosc;
        int y = (int) o.wartosc;
        if(x < y) {
            return -1;
        }
        if(x > y) {
            return 1;
        }
        return 0;
    }

    public String toString(){
        return wartosc.toString();
    }

    public T wartosc;
}
